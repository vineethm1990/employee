import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { signInRequest } from "../../store/modules/auth/actions";

export default function Login({ match }) {
  const { error, loading } = useSelector(state => state.auth);

  const formData = {
    formIsValid: false,
    Username: "",
    Password: "",
  };

  const dispatch = useDispatch();

  const [login, setLogin] = useState(formData);
  const [emailError,setEmailerror]= useState('');
  const [passwordError,setpasswordError]= useState('');

  useEffect(() => {
  }, [login]);

  function handleChange(e) {
    setLogin({ ...login, [e.target.name]: e.target.value });
  }

  function handleSubmit() {
    const { Username, Password } = login;
    var emailValid = Username.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    var fieldValidationErroremail = emailValid ? '' : 'invalid';

    if(fieldValidationErroremail=="invalid"){
      setEmailerror('Please enter a valid email');
    }
    else if(Password==''){
      setpasswordError('Please enter a password');
    }
    else{
      dispatch(signInRequest(Username, Password));      
    }
    return false;
  }

  console.log(error);

  return (
    <div class="wrapper fadeInDown">
      <div id="formContent">
        <div class="fadeIn first">
          <h3>Login</h3>
        </div>
        <form>
          {(error != null && error != 0) && <h6 style={{ color: 'red' }}>{error.error}</h6>}
          <input type="text" id="Username" class="fadeIn second" name="Username" placeholder="Username" onChange={handleChange} />
          {emailError != ''&&<h6 style={{ color: 'red' }}>{emailError}</h6>}
          <input type="text" id="Password" class="fadeIn third" name="Password" placeholder="Password" onChange={handleChange} />
          {passwordError != ''&&<h6 style={{ color: 'red' }}>{passwordError}</h6>}
          <input type="button" class="fadeIn fourth" value="Log In" onClick={() => handleSubmit()} />
        </form>
      </div>
    </div>
  );
}
