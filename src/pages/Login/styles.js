import styled from "styled-components";

export const Container = styled.div`
  label {
    margin-top: 0;
  }
  // background-image: linear-gradient(to left top, #0582ff, #218eff, #3799ff, #4ba4ff, #5fafff);
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  article {
    display: inline-block;
    width: 35%;
    margin-left: 120px;
    h1 {
      font-size: 48px;
      color: #fff;
    }
    p {
      margin-top: 20px;
      font-size: 16px;
      color: #fff;
    }
  }
  button {
    margin-left: auto;
    margin-top: 50px;
    margin-bottom: 30px;
  }
  @media screen and (max-width: 895px) {
    width: 100%;
    height: 100%;
    display: flex;
    padding: 50px 0;
    flex-direction: column-reverse;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    button {
      margin-top: 0;
    }

    article {
      margin-left: 0;
      width: 100%;
      margin-bottom: 20px;
      font-size: 24px;
      h1 {
        font-size: 34px;
      }
    }
  }
`;

export const AlertBox = styled.div`
  background: #d34836;
  padding: 10px;
  color: white;
  border-radius: 5px;
  margin-top: 10px;
`;
