import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { employeeList } from "../../store/modules/dashboard/actions";

export default function Dashboard({ match }) {
    const { employee } = useSelector(state => state.dashboard);

    const dispatch = useDispatch();
    var employees=[];

    useEffect(() => {
        dispatch(employeeList());
    }, []);
    if (employee != null) {
        employees = employee.map((items, key) => {
            return (
                <tr>
                    <td>{items.id}</td>
                    <td>{items.name}</td>
                    <td>{items.age}</td>
                    <td>{items.gender}</td>
                    <td>{items.email}</td>
                    <td>{items.phoneNo}</td>
                </tr>
            );
        })
    }

    return (
        <div class="container">
            <div class="row">
                <div class="span5">
                    <table class="table table-striped table-condensed">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Name</th>
                                <th>Age</th>
                                <th>Gender</th>
                                <th>Email</th>
                                <th>PhoneNo</th>
                            </tr>
                        </thead>
                        <tbody>
                          {employees}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

