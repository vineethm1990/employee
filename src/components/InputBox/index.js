import React from "react";
import { Input } from "./styles";

export default function InputBox({
  type,
  children,
  placeholder,
  padding,
  width,
  margin,
  bg,
  containerWidth,
  onChange,
  name,
  masked,
  mask,
  value,
  password,
  awesome,
  problem
}) {
  return (
    <Input
      awesome={awesome}
      margin={margin}
      type={type}
      label={children}
      placeholder={placeholder}
      padding={padding}
      width={width}
      name={name}
      bg={bg}
      mask={mask}
      onChange={onChange}
      value={value}
      password={password}
      problem={problem}
      containerWidth={containerWidth}
    />
  );
}
