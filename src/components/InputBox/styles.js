import styled from "styled-components";
import React from "react";
import colors from "~/global/colors";
import MaskedInput from "react-maskedinput";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  width: ${props =>
    props.containerWidth ? props.containerWidth + "px" : "100%"};
  margin-right: 3px;
  input {
    ${props => (props.margin ? "margin-right: 20px;" : "")}
    font-size: 16px;
    line-height: 17px;
    width: 100%;
    border: none;
    border-radius: 4px;
    position: relative;
    background: url(${props => props.bg});
    background-repeat: no-repeat;
    background-position: 20px center;
    margin: 13px 5px;
    color: ${colors.midBlack};
    &::placeholder {
      color: ${colors.midGray};
    }
  }
  @media screen and (max-width: 768px) {
    width: 100%;
    margin-right: 0;
  }
`;

export const Label = styled.label`
  display: ${props => (props.label ? "block" : "none")};
  color: ${props => (props.problem ? colors.red : colors.midGray)};
  font-size: 14px;
`;

export const Small = styled.small`
  color: ${colors.red};
  font-size: 9px;
  margin-left: 5px;
`;

export const Input = ({
  margin,
  type,
  label,
  placeholder,
  padding,
  width,
  containerWidth,
  bg,
  onChange,
  name,
  masked,
  mask,
  value,
  password,
  awesome,
  problem
}) => {
  if (masked === false) {
    return (
      <Container
        containerWidth={containerWidth}
        margin={margin}
        bg={bg}
        padding={padding}
        width={width}
        onChange={onChange}
      >
        <Label problem={problem} label={label}>
          {label}
          <Small>{problem}</Small>
        </Label>
        <InputBox>
          {awesome && <FontAwesomeIcon icon={awesome} color={"#545C56"} />}
          <input
            onChange={onChange}
            name={name}
            type={password ? "password" : type}
            placeholder={placeholder}
            value={value}
          />
        </InputBox>
      </Container>
    );
  } else {
    return (
      <Container
        containerWidth={containerWidth}
        margin={margin}
        bg={bg}
        padding={padding}
        width={width}
        onChange={onChange}
      >
        <Label>{label}</Label>
        <InputBox>
          {awesome && <FontAwesomeIcon icon={awesome} color={"#545C56"} />}
        </InputBox>
      </Container>
    );
  }
};

export const InputBox = styled.div`
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  padding: 0 15px;
  border: solid 1px #ddd;
  border-radius: 5px;
  margin: 8px 0;
`;

export const OldInput = styled.input``;

export const InputItem = styled.input`
  width: 100%;
`;
