import styled from "styled-components";
import colors from "~/global/colors";

export const Container = styled.button`
  min-width: ${props => `${props.width}px`};
  border-radius: 25px;
  background: ${props => props.background};
  padding: ${props => (props.padding ? props.padding : '0 20px')};
  ${props => (props.toggle ? 'padding-right: 56px;' : '')}
  text-align: center;
  height: ${props => (props.height ? props.height : "44px")};
  margin: ${props => (props.margin ? props.margin : "0")}px 0;
  font-size: 14px;
  font-weight: ${props =>
    props.styled || props.background === colors.primaryPink ? "400" : "700"};
  color: ${props => (props.color ? props.color : "#fff")};
  border: ${props => (props.border ? props.border : "none")};
  display: block;
  text-transform: ${props =>
    props.styled || props.background === colors.primaryPink ? "unset" : "uppercase"};
  -webkit-font-smoothing: antialiased;
  /* letter-spacing: 1.8px; */
  cursor: pointer;
  transition: .2s all linear;
  position: relative;
  &:hover {
    background: ${props => props.background};
    color: ${props => (props.color ? props.color : "#fff")};
    opacity: 0.8;
  }
  &:active {
    ${props =>
      props.bgActive
        ? `background: ${props => props.bgActive}; color: #fff; border: none; opacity: 0.5;`
        : ""}
  }
  ${props =>
    props.toggle ? `
      &::after{
        content: "";
        width: 8px;
        height: 8px;
        background-color: transparent;
        border: 2px solid ${props.color};
        border-left: none;
        border-bottom: none;
        position: absolute;
        transform: rotate(${props.toggleActive ? `135deg` : `-45deg` });
        top: calc(50% - ${props.toggleActive ? `5px` : `2px` });
        right: 24px;
      }
    `: ""
  }
`;
