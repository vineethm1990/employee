import React from "react";
import { Container } from "./styles";

const Button = ({
  children,
  padding,
  bgActive,
  width,
  background,
  height,
  border,
  color,
  onClick,
  styled,
  margin,
  toggle,
  toggleActive
}) => {
  return (
    <Container
      padding={padding}
      background={background}
      height={height}
      width={width}
      bgActive={bgActive}
      border={border}
      color={color}
      onClick={onClick}
      styled={styled}
      margin={margin}
      toggle={toggle}
      toggleActive={toggleActive}
    >
      {children}
    </Container>
  );
};

export default Button;
