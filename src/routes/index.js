import React from "react";
import { BrowserRouter, Switch, Router } from "react-router-dom";

import Route from "./Route";

import Login from "../pages/Login";
import Dashboard from "../pages/Dashboard";
import history from "../services/history";

export default function routes() {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={Login} />
        <Route path="/dashboard" exact component={Dashboard} /> 
      </Switch>
    </Router>
  );
}
