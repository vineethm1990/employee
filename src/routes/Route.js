import React from "react";
import PropTypes from "prop-types";
import { Route as RouteDom, BrowserRouter, Redirect } from "react-router-dom";
import { store } from "../store";

export default function Route({ component: Component, isPrivate, ...rest }) {
  const { signed, user } = store.getState().auth;

  if (!signed && isPrivate) {
    return <Redirect to="/login" />;
  }

  // if (signed && !isPrivate) {
  //   return <Redirect to={`/managers/dashboard`} />;
  // }

  return <RouteDom {...rest} component={Component} />;
}

Route.propTypes = {
  isPrivate: PropTypes.bool,
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func, PropTypes.object]).isRequired
};

Route.defaultProps = {
  isPrivate: false
};
