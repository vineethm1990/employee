import produce from "immer";

const INITIAL_STATE = {

  };

export default function dashboard(state = INITIAL_STATE, action) {
    switch (action.type) {  
      case "@dashboard/EMPLOYEE_LIST":
        return produce(state, draft => {
          draft.loading = true;
          draft.error = null;
        });
        case "@dashboard/EMPLOYEE_LIST_SUCCESS":
            return produce(state, draft => {
              draft.employee = action.payload.employeelist;
            });
  
      default:
        return state;
    }
  }