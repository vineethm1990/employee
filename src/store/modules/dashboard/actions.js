export function employeeList() {
    return {
      type: "@dashboard/EMPLOYEE_LIST",
      payload: {}
    };
  }

export function employeeListSuccess(employeelist){
    return {
        type: "@dashboard/EMPLOYEE_LIST_SUCCESS",
        payload: {employeelist}
      }; 
}