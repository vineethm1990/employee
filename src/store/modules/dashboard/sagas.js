import {employeeListDetails} from './script.js';
import { takeLatest, call, put, all } from "redux-saga/effects";

import { employeeListSuccess } from "./actions";

export function* employeeList({ payload }) {


      yield put(employeeListSuccess(employeeListDetails));

}

export default all([
    takeLatest("@dashboard/EMPLOYEE_LIST", employeeList),
  ]);
  