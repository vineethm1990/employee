import { takeLatest, call, put, all } from "redux-saga/effects";


import {loginDetails} from './script'

import { signInSuccess, signFailure } from "./actions";

export function* signIn({ payload }) {

    let { username, password } = payload;
    username = username
      .replace(/\(/g, "")
      .replace(/\)/g, "")
      .replace(/ /g, "")
      .replace(/-/g, "");

    if(loginDetails.username == username && loginDetails.password == password)
    {
      yield put(signInSuccess('Login Success'));
    }
    else{
      yield put(signFailure("Username/Password doesnt match"));
    }

}


export default all([
  takeLatest("@auth/SIGN_IN_REQUEST", signIn),
]);
