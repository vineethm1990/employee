// sign in
export function signInRequest(username, password) {
  return {
    type: "@auth/SIGN_IN_REQUEST",
    payload: { username, password }
  };
}

export function signInSuccess(access_token, user) {
  return {
    type: "@auth/SIGN_IN_SUCCESS",
    payload: { access_token, user }
  };
}

export function signFailure(error) {
  return { type: "@auth/SIGN_IN_FAILURE", payload: { error } };
}


