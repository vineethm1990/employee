import produce from "immer";

const INITIAL_STATE = {
  signed: false,
  loading: false,
  error: null
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {  
    case "@auth/SIGN_IN_REQUEST":
      return produce(state, draft => {
        draft.loading = true;
        draft.error = null;
      });
    case "@auth/SIGN_IN_SUCCESS":
      return produce(state, draft => {
        draft.signed = true;
        draft.error = null;
      });
    case "@auth/SIGN_IN_FAILURE":
      return produce(state, draft => {
        draft.error = action.payload;
      });

    default:
      return state;
  }
}
